import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Artist from "./containers/Artist/Artist";
import InfoAlbums from "./components/InfoAlbums/InfoAlbums";
import InfoTracks from "./components/InfoTracks/InfoTracks";

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Toolbar/>
            </header>
            <Container style={{marginTop: '20px'}}>
                <Switch>
                    <Route path="/" exact component={Artist}/>
                    <Route path="/infoAlbums/:id" exact component={InfoAlbums}/>
                    <Route path="/infoTrack/:id" exact component={InfoTracks}/>
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;
