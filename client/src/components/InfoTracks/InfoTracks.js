import React, {Component} from 'react';
import {connect} from "react-redux";
import {infoTrack} from "../../store/actions/actionsAlbums";
import TracksListItems from "../TracksListItems/TracksListItems";

class InfoTracks extends Component {
    componentDidMount() {
        this.props.infoTrack(this.props.match.params.id)
    }

    render() {
        return (
            this.props.infoTracks ? <div>
                <h2>Tracks</h2>
                {this.props.infoTracks.map(tracks => (
                    <TracksListItems
                         key={tracks._id}
                         trackNumber={tracks.trackNumber}
                         nameTrack={tracks.nameTrack}
                         longest={tracks.longest}
                    />
                ))}
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    infoTracks: state.albums.infoTracks
});

const mapDispatchToProps = dispatch => ({
    infoTrack: id => dispatch(infoTrack(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoTracks);