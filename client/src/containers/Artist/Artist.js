import React, {Component} from 'react';
import {fetchArtist} from "../../store/actions/actionsArtist";
import {connect} from "react-redux";
import ArtistListItems from "../../components/ArtistListItems/ArtistListItems";

class Artist extends Component {

    componentDidMount() {
        this.props.onFetchArtist();
    }

    render() {
        return (
            <div>
                <h2>Artist</h2>
                {this.props.artist.map(artist => (
                    <ArtistListItems
                        key={artist._id}
                        _id={artist._id}
                        name={artist.name}
                        image={artist.image}
                    />
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    artist: state.artist.artist
});

const mapDispatchToProps = dispatch => ({
    onFetchArtist: () => dispatch(fetchArtist())
});

export default connect(mapStateToProps, mapDispatchToProps)(Artist);