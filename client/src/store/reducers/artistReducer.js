import {FETCH_ALBUMS_FAILURE, FETCH_ARTIST_FAILURE, FETCH_ARTIST_SUCCESS} from "../actions/actionsArtist";
import {INFO_TRACKS_SUCCESS} from "../actions/actionsAlbums";

const initialState = {
    artist: [],
    infoAlbum: null,
    error: null,
};

const artistReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ARTIST_SUCCESS:
            return {...state,
                artist: action.artist
            };

        case INFO_TRACKS_SUCCESS:
            return {
                ...state,
                infoAlbum: action.data
            };

        case FETCH_ARTIST_FAILURE:
            return {
                ...state,
                error: action.error
            };

        case FETCH_ALBUMS_FAILURE:
            return {
                ...state,
                error: action.error
            };

        default:
            return state;
    }
};

export default artistReducer;