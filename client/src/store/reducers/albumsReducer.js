import {FETCH_TRACKS_FAILURE, INFO_TRACKS_SUCCESS} from "../actions/actionsAlbums";
import {FETCH_ALBUMS_SUCCESS} from "../actions/actionsArtist";

const initialState = {
    albums: [],
    infoTracks: null,
    error: null
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ALBUMS_SUCCESS:
            return {
                ...state,
                albums: action.data
            };

        case INFO_TRACKS_SUCCESS:
            return {
                ...state,
                infoTracks: action.data
            };

        case FETCH_TRACKS_FAILURE:
            return {
                ...state,
                error: action.error
            };

        default:
            return state;
    }
};

export default albumsReducer;