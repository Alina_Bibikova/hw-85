import axios from '../../axios-api';

export const FETCH_ARTIST_SUCCESS = 'FETCH_ARTIST_SUCCESS';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ARTIST_FAILURE = 'FETCH_ARTIST_FAILURE';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const fetchArtistSuccess = artist => ({type: FETCH_ARTIST_SUCCESS, artist});
export const infoAlbumsSuccess = data => ({type: FETCH_ALBUMS_SUCCESS, data});
export const fetchArtistFailure = error => ({type: FETCH_ARTIST_FAILURE, error});
export const fetchAlbumsFailure = error => ({type: FETCH_ALBUMS_FAILURE, error});

export const fetchArtist = () => {
    return dispatch => {
        return axios.get('/artist').then(
            response => dispatch(fetchArtistSuccess(response.data))
        ).catch(error => dispatch(fetchArtistFailure(error)));
    };
};

export const infoAlbums = id => {
    return dispatch => {
        return axios.get(`/albums?artist=${id}`).then(
            response => dispatch(infoAlbumsSuccess(response.data))
        ).catch(error => dispatch(fetchAlbumsFailure(error)));
    };
};
