import axios from '../../axios-api';

export const INFO_TRACKS_SUCCESS = 'INFO_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';

export const infoTracksSuccess = data => ({type: INFO_TRACKS_SUCCESS, data});
export const fetchTracksFailure = error => ({type: FETCH_TRACKS_FAILURE, error});

export const infoTrack = id => {
    return dispatch => {
        return axios.get(`/tracks?albums=${id}`).then(
            response => dispatch(infoTracksSuccess(response.data))
        ).catch(error => dispatch(fetchTracksFailure(error)));
    };
};
