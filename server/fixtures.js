const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Albums = require('./models/Album');
const Tracks = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const [johny, adele, lana] = await Artist.create(
        {   name: 'Johnny Cash',
            description: 'Hurt',
            image: 'johnnyCash.jpg'

        },
        {
            name: 'Adele',
            description: 'Someone Like You',
            image: 'adel.jpeg'
        },
        {
            name: 'Lana Del Rey',
            description: 'Ride',
            image: 'lana.jpg'
        },
    );

    const [album1, album2, album3, album4, album5] =  await Albums.create(
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2002,
            image: 'cash.jpg'
        },
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2007,
            image: 'cash.jpg'
        },
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2004,
            image: 'cash.jpg'
        },
        {   nameAlbums: 'Adele 21',
            nameArtist: adele._id,
            date: 2011,
            image: 'adele.jpg'
        },

        {   nameAlbums: 'Born to Die',
            nameArtist: lana._id,
            date: 2012,
            image: 'born.jpg'
        },
    );

    await Tracks.create(
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 1
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 3
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 2
        },

        {   nameTrack: 'Someone Like You',
            nameAlbum: album4._id,
            longest: '03:30',
            trackNumber: 1
        },

        {   nameTrack: 'Born to Die',
            nameAlbum: album5._id,
            longest: '03:30',
            trackNumber: 1
        },
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});