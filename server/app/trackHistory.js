const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Token');
    const user = await User.findOne({token: token});
    if (!user) {
        return res.status(401).send({error: 'Unauthorized'});
    }

    const trackHistory = new TrackHistory({
        user: user._id,
        track: req.body.track,
        datetime: new Date().toISOString()
    });

        trackHistory.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

module.exports = router;