const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const Artist = require('../models/Artist');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', (req, res) => {
    Artist.find().populate('albums')
        .then(artist => res.send(artist))
        .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('image'), (req, res) => {
    const artistData = req.body;

    if (req.file) {
        artistData.image = req.file.filename;
    }

    const artist = new Artist(artistData);

    artist.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.put('/:id', upload.single('image'), async (req, res) => {
    try {
        const updateArtist = await Artist.findById(req.params.id);
        updateArtist.name = req.body.name;
        updateArtist.description = req.body.description;
        updateArtist.image = req.body.image;

        if (req.file) {
            updateArtist.image = req.file.filename;
        }

        await updateArtist.save();
        return res.send(updateArtist);
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await Artist.deleteOne({_id: req.params.id});
        return res.sendStatus(200);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;